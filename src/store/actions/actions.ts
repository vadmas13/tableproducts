import { Commit  } from 'vuex';


export default {
  GET_PRODUCTS( { commit }: { commit: Commit }){
   commit('GET_PRODUCTS_MUTATION');
  }
}
