import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

import newPostActions from "./actions/actions";

import newPostMutations from "./mutations/mutations";

import getters from './getters/getters'
import state from './state'


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    ...state
  },
  mutations: {
    ...newPostMutations
  },
  actions: {
    ...newPostActions
  },
  getters: {
    ...getters
  },
  plugins: [
   /* createPersistedState({
      paths: ['posts']
    })*/
  ]
});
