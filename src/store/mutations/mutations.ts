import stateTypes from './../../types/state'
import { getProducts } from '../../initData/request'

export default {
  GET_PRODUCTS_MUTATION: async (state: stateTypes) => {
    let resolveOrReject: any  = await getProducts();
    if(resolveOrReject.error){
      state.error = 'Server error'
    }else{ debugger;
      state.products = resolveOrReject
      state.error = ''
    }
  }
}
