interface State {
  products: Array<ProductItemType>
  error: string
}

export type ProductItemType = {
  "id": number
  "product": string
  "calories": number
  "fat": number
  "carbs": number
  "protein":number
  "iron": number
}


export default State
